﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.ViewModels
{
    public class IndexViewModel
    {
        public List<Models.Client> DisplayableClients
        {
            get;
            set;
        }

        public Models.Client CreatableClient
        {
            get;
            set;
        }

        public string APIResultString
        {
            get;
            set;
        }
    }
}