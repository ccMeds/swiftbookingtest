﻿using System.Runtime.Serialization;

// Consider moving Requests/Details to another project, perhaps a models project the Client model can also be a part of?
// Worth considering to seperate front end and backend further.
namespace SwiftBookingTest.Web.Requests
{
    [DataContract]
    public class SwiftRequest
    {
        [DataMember(Name = "apiKey")]
        public string ApiKey = "3285db46-93d9-4c10-a708-c2795ae7872d";
    }
}