﻿using System.Runtime.Serialization;
using SwiftBookingTest.Web.Requests.Details;

namespace SwiftBookingTest.Web.Requests
{
    [DataContract]
    internal class BookingRequest : SwiftRequest
    {
        public BookingRequest(string _pickUpAddress, string _dropOffAddress)
        {
            Booking = new BookingDetail(_pickUpAddress, _dropOffAddress);
        }

        [DataMember(Name = "Booking")]
        public BookingDetail Booking
        {
            get;
            set;
        }
    }
}