﻿using System.Runtime.Serialization;

namespace SwiftBookingTest.Web.Requests.Details
{
    [DataContract]
    class AddressDetail
    {
        public AddressDetail()
        {

        }
        public AddressDetail(string _address)
        {
            Address = _address;
        }

        [DataMember(Name = "address")]
        public string Address
        {
            get;
            set;
        }
    }
}