﻿using System.Runtime.Serialization;

namespace SwiftBookingTest.Web.Requests.Details
{
    [DataContract]
    class BookingDetail// : IBookingDetail
    {
        public BookingDetail(string _pickUpAddress, string _dropOffAddress)
        {
            PickUpDetail = new AddressDetail(_pickUpAddress);
            DropoffDetail = new AddressDetail(_dropOffAddress);
        }

        [DataMember(Name = "dropoffDetail")]
        public AddressDetail DropoffDetail
        {
            get;
            set;
        }

        [DataMember(Name = "pickUpDetail")]
        public AddressDetail PickUpDetail
        {
            get;
            set;
        }
    }
}