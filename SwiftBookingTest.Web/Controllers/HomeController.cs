﻿using SwiftBookingTest.Web.DAL;
using SwiftBookingTest.Web.Models;
using SwiftBookingTest.Web.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Reflection;
using System.Net;
using System.IO;

using SwiftBookingTest.Web.Requests;

namespace SwiftBookingTest.Web.Controllers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class MultipleButtonAttribute : ActionNameSelectorAttribute
    {
        public string Name { get; set; }
        public string Argument { get; set; }


        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            var isValidName = false;
            var keyValue = string.Format("{0}:{1}", Name, Argument);
            var value = controllerContext.Controller.ValueProvider.GetValue(keyValue);

            if (value != null)
            {
                controllerContext.Controller.ControllerContext.RouteData.Values[Name] = Argument;
                isValidName = true;
            }

            return isValidName;
        }
    }

    public class HomeController : Controller
    {
        private ClientsContext db = new ClientsContext();


        public HomeController() : base()
        {

        }
        //
        // GET: /Client/

        public ActionResult Index()
        {
            return View(BuildBasicIndexViewModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Add")]
        public ActionResult Add(IndexViewModel indexViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Clients.Add(indexViewModel.CreatableClient);
                db.SaveChanges();
            }

            indexViewModel = new IndexViewModel();
            indexViewModel.DisplayableClients = db.Clients.ToList();
            return View("Index", BuildBasicIndexViewModel());
        }
                
        public ActionResult Upload(int id = 0)
        {
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }

            string pickUp = "105 collins st, 3000";
            string dropOff = db.Clients.Where((x) => x.ID == id).First().Address;

            var bookingRequest = new BookingRequest(pickUp, dropOff);

            string json;
            using (var ms = new MemoryStream())
            {
                var ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(BookingRequest));
                ser.WriteObject(ms, bookingRequest);
                json = System.Text.Encoding.UTF8.GetString(ms.GetBuffer(), 0, Convert.ToInt32(ms.Length));
            }


            string resultText = "";
            using (WebClient webClient = new WebClient())
            {
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
                webClient.UseDefaultCredentials = true;
                webClient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                try
                {
                    resultText = webClient.UploadString("https://app.getswift.co/api/v2/deliveries", "POST", json);
                }
                catch (Exception E)
                {
                    // No requirements given for how to handle bad requests, just display error for now as that is technically the result of the call.
                    resultText = E.Message;
                }
            }

            return View("Index", BuildBasicIndexViewModel(resultText));
        }

        IndexViewModel BuildBasicIndexViewModel(string resultString ="")
        {
            var vM = new IndexViewModel() { DisplayableClients = db.Clients.ToList(), APIResultString = resultString };
            return vM;
        }
    }
}
