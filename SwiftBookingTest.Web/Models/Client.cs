﻿
// might make sense to move this guy into a models project?
namespace SwiftBookingTest.Web.Models
{
    public class Client
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}