﻿using SwiftBookingTest.Web.Models;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;



namespace SwiftBookingTest.Web.DAL
{
    public class ClientsContext : DbContext
    {
        public ClientsContext() : base("DefaultConnection")
        {

        }

        public DbSet<Client> Clients { get; set; }
    }
}