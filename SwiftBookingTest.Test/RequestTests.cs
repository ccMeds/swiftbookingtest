﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using SwiftBookingTest.Web.Requests;
namespace SwiftBookingTest.Test
{
    // todo: moar tests - should have taken TDD or BDD approach :)
    [TestClass]
    public class RequestTests
    {
        [TestMethod]
        public void BookingRequestSanityCheck()
        {
            const string pickUpAddress = "123 Fake St, Springfield";
            const string dropOffAddress = "742 Evergreen Terrace, Springfield";

            BookingRequest request = new BookingRequest(pickUpAddress, dropOffAddress);


            Assert.IsFalse(string.IsNullOrEmpty(request.ApiKey));

            Assert.IsTrue(request.Booking.PickUpDetail.Address == pickUpAddress);
            Assert.IsTrue(request.Booking.DropoffDetail.Address == dropOffAddress);
        }
    }
}